package sulthon.com.kalkulatordiskon;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText hargaBruto;
    TextView displayHarga;

    double hargaBarang, hargaNetto, potongan;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        displayHarga = (TextView) findViewById(R.id.tv_display);
        hargaBruto = (EditText) findViewById(R.id.et_hargaBruto);
        Button _10persen = (Button) findViewById(R.id.bt_10persen);
        Button _20persen = (Button) findViewById(R.id.bt_20persen);
        Button _30persen = (Button) findViewById(R.id.bt_30persen);
        Button reset = (Button) findViewById(R.id.bt_reset);

        _10persen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double potongan1;
                hargaBarang = Integer.parseInt(String.valueOf(hargaBruto.getText()));
                potongan = (int) (hargaBarang * (0.1));
                potongan1 = Double.parseDouble(String.valueOf(potongan));
                hargaNetto = (int) (hargaBarang - potongan1);
                displayHarga.setText(String.valueOf(hargaNetto));
            }
        });

        _20persen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hargaBarang = Integer.parseInt(String.valueOf(hargaBruto.getText()));
                potongan = hargaBarang * (0.2);
                hargaNetto = hargaBarang - potongan;
                displayHarga.setText(String.valueOf(hargaNetto));
            }
        });

        _30persen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hargaBarang = Integer.parseInt(String.valueOf(hargaBruto.getText()));
                potongan = hargaBarang * (0.3);
                hargaNetto = hargaBarang - potongan;
                displayHarga.setText(String.valueOf(hargaNetto));
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hargaNetto = 0;
                displayHarga.setText(0);
            }
        });
    }
}
